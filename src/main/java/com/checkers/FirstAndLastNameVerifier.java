package com.checkers;

/*
Sprawdzamy czy nie ma spacji w dwuczłonowym imieniu lub nazwisku
Interesuje nas tylko pierwsze imie, imie Jan-Maria lub nazwisko panieńskie-nazwisko
 */
public class FirstAndLastNameVerifier implements AccountVerifier {

    public boolean isAdequate(String Name) {
            if(Name.equals("")){
                System.out.println("\tNie wprowadzono wymaganych danych!!!");
                return false;
            }

        int first = Name.trim().length();

        String ReplacedName = Name.replaceAll("[^A-Za-z]+", "");

        int second = ReplacedName.length();

        if (first != second) {
            System.out.println("\tNie wprowadzono danych w wymaganej formie!!!");
            return false;
        } else return true;
    }

}
